package cardservice.model;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InventoryTest {
	private List<Card> cardList;
	private List<Integer> userIdList;
	private List<Integer> idList;


	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add Inventory to test");
		cardList = new ArrayList<Card>();
		userIdList = new ArrayList<Integer>();
		idList = new ArrayList<Integer>();
		Card c = new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100);
		Card c2 = new Card("batman", "super carte", "heroes", 10, 100, 100, 50, "http://url.com", 1000);
		cardList.add(c);
		cardList.add(c2);
		userIdList.add(1);
		userIdList.add(2);
		idList.add(1);
		idList.add(4);
		idList.add(20);

	}
	
	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN hero list");
		cardList = null;
		userIdList = null;
	}
	
	@Test
	public void createInventory() {
		
		for(Integer id:userIdList) {
			for(Integer userId:userIdList) {
				Inventory i = new Inventory(id, cardList, userId);
				System.out.println("id:"+id+", userId:"+userId+", list:"+cardList);
				assertTrue(i.getId() == id.intValue());
				assertTrue(i.getCards() == cardList);
				assertTrue(i.getUserId() == userId.intValue());
			}
		}
	}
	
}
