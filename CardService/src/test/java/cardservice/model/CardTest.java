package cardservice.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CardTest {
	private List<String> stringList;
	private List<Integer> intList;

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add Card to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
	}
	
	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN hero list");
		stringList = null;
		intList = null;
	}
	
	@Test
	public void createCard() {
		for(String msg:stringList) {
			for(String msg2:stringList) {
				for(String msg3:stringList) {
					for(Integer msg4:intList) {
						Card c=new Card(msg, msg2, msg3, msg4, msg4, msg4, msg4, msg2, msg4);
						System.out.println("msg:"+msg+", msg2:"+msg2+", msg3:"+msg3+", msg4:"+msg4);
						assertTrue(c.getName() == msg);
						assertTrue(c.getDescription() == msg2);
						assertTrue(c.getFamily() == msg3);
						assertTrue(c.getHp() == msg4.intValue());
						assertTrue(c.getEnergy() == msg4.intValue());
						assertTrue(c.getDefence() == msg4.intValue());
						assertTrue(c.getAttack() == msg4.intValue());
						assertTrue(c.getImgUrl() == msg2);
						assertTrue(c.getPrice() == msg4.intValue());
					}	
				}	
			}
		}
	}


}
