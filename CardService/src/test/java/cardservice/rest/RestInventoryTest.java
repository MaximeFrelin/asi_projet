package cardservice.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cardservice.model.Card;
import cardservice.model.Inventory;
import cardservice.service.InventoryService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RestInventory.class)
public class RestInventoryTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private InventoryService iService;
	
	Card c1 = new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100);
	Card c2 = new Card("batman", "super carte", "heroes", 50, 100, 90, 50, "http://url.com", 200);
	List<Card> mockAllCards = new ArrayList<>();
	Inventory mockInventory = new Inventory(1, mockAllCards, 1);
	
	@Test
	public void retrieveInventoy() throws Exception {
		mockAllCards.add(c1);
		mockAllCards.add(c2);
		
		Mockito.when(iService.get(Mockito.anyInt())).thenReturn(mockInventory);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/inventory/1").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expectedResult = "{\"id\":1,\"userId\":1,\"cards\":[{\"id\":0,\"name\":\"superman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":100,\"energy\":50,\"defence\":50,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":100},{\"id\":0,\"name\":\"batman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":50,\"energy\":100,\"defence\":90,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":200}]}";
		String resultTest = result.getResponse().getContentAsString(); 	
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
}
