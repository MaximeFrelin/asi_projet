package cardservice.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cardservice.model.Card;
import cardservice.service.CardService;


@RunWith(SpringRunner.class)
@WebMvcTest(value = RestCard.class)
public class RestCardTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CardService cService;
	
	Card mockCard = new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100);
	Card mockCard2 = new Card("batman", "super carte", "heroes", 50, 100, 90, 50, "http://url.com", 200);
	List<Card> mockAllCards = new ArrayList<>();
	
	@Test
	public void retrieveAllCards() throws Exception {
		mockAllCards.add(mockCard);
		mockAllCards.add(mockCard2);

		Mockito.when(cService.getAll()).thenReturn(mockAllCards);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/card/list").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
		String expectedResult = "[{\"id\":0,\"name\":\"superman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":100,\"energy\":50,\"defence\":50,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":100},{\"id\":0,\"name\":\"batman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":50,\"energy\":100,\"defence\":90,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":200}]";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
	
	@Test
	public void retrieveOneCard() throws Exception {
		Mockito.when(cService.getCard(Mockito.anyInt())).thenReturn(mockCard);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/card/1").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
		String expectedResult = "{\"id\":0,\"name\":\"superman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":100,\"energy\":50,\"defence\":50,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":100}";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
	
	@Test
	public void searchOneCard() throws Exception {
		mockAllCards.add(mockCard);
		mockAllCards.add(mockCard2);
		
		Mockito.when(cService.searchCard(Mockito.anyString())).thenReturn(mockAllCards);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/card/search/a").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
		String expectedResult = "[{\"id\":0,\"name\":\"superman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":100,\"energy\":50,\"defence\":50,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":100},{\"id\":0,\"name\":\"batman\",\"description\":\"super carte\",\"family\":\"heroes\",\"hp\":50,\"energy\":100,\"defence\":90,\"attack\":50,\"imgUrl\":\"http://url.com\",\"price\":200}]";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
}
