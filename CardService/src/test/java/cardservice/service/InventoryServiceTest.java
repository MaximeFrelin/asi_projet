package cardservice.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import cardservice.model.Card;
import cardservice.model.Inventory;
import cardservice.repository.CardRepository;
import cardservice.repository.InventoryRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = InventoryService.class)
public class InventoryServiceTest {
	
	@Autowired
	private InventoryService iService;
	
	@MockBean
	private InventoryRepository iRepository;
	
	Card c1 = new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100);
	Card c2 = new Card("batman", "super carte", "heroes", 50, 100, 90, 50, "http://url.com", 200);
	List<Card> mockAllCards = new ArrayList<>();
	Optional<Inventory> mockInventory = Optional.ofNullable(new Inventory(1, mockAllCards, 1));
	
	@Test
	public void get() {
		mockAllCards.add(c1);
		mockAllCards.add(c2);

		Mockito.when(iRepository.findByUserId(Mockito.anyInt())).thenReturn(mockInventory);
		
		Inventory result = iService.get(0);
		
		assertTrue(result.getId() == mockInventory.get().getId());
		assertTrue(result.getUserId() == mockInventory.get().getUserId());

	}
}
