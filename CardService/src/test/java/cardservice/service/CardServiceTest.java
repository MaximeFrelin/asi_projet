package cardservice.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import cardservice.model.Card;
import cardservice.repository.CardRepository;


@RunWith(SpringRunner.class)
@WebMvcTest(value = CardService.class)
public class CardServiceTest {

	@Autowired
	private CardService cService;
	
	@MockBean
	private CardRepository cRepository;
	
	Card c1 = new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100);
	Card c2 = new Card("batman", "super carte", "heroes", 50, 100, 90, 50, "http://url.com", 200);
	List<Card> mockAllCards = new ArrayList<>();
	
	@Test
	public void getAll() {
		mockAllCards.add(c1);
		mockAllCards.add(c2);
		
		Mockito.when(cRepository.findAll()).thenReturn(mockAllCards);
		
		List<Card> cards = cService.getAll();
		
		assertTrue(cards.toString().contentEquals(mockAllCards.toString()));
	}
	
	@Test
	public void getCard() {
		Optional<Card> optC1 = Optional.ofNullable(new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100));
				
		Mockito.when(cRepository.findById(Mockito.anyInt())).thenReturn(optC1);
		
		Card result = cService.getCard(1);
		
		assertTrue(result.getName().equals(optC1.get().getName()));
		
	}
	
	@Test
	public void searchCard() {
		mockAllCards.add(c1);
		
		Mockito.when(cRepository.findByName(Mockito.anyString())).thenReturn(mockAllCards);
		
		List<Card> cards = cService.searchCard("hey");
		
		assertTrue(cards.toString().contentEquals(mockAllCards.toString()));
		
	}
}
