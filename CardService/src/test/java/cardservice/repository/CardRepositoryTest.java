package cardservice.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import cardservice.model.Card;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CardRepositoryTest {
	@Autowired
	CardRepository crepo;

	@Before
	public void setUp() {
		crepo.save(new Card("superman", "super carte", "heroes", 100, 50, 50, 50, "http://url.com", 100));
	}

	@After
	public void cleanUp() {
		crepo.deleteAll();
	}
	
	@Test
	public void saveCard() {
		crepo.save(new Card("batman", "super carte", "heroes", 10, 100, 100, 50, "http://url.com", 1000));
		assertTrue(true);
	}

	@Test
	public void saveAndGetHero() {
		crepo.deleteAll();
		crepo.save(new Card("batman", "super carte", "heroes", 10, 100, 100, 50, "http://url.com", 1000));
		List<Card> cardList = new ArrayList<>();
		crepo.findAll().forEach(cardList::add);
		assertTrue(cardList.size() == 1);
		assertTrue(cardList.get(0).getName().equals("batman"));
		assertTrue(cardList.get(0).getDescription().equals("super carte"));
		assertTrue(cardList.get(0).getFamily().equals("heroes"));
		assertTrue(cardList.get(0).getImgUrl().equals("http://url.com"));
		System.out.println("name"+cardList.get(0).getName());
	}
	
	@Test
	public void getCard() {
		List<Card> cardList = crepo.findByName("superman");
		assertTrue(cardList.size() == 1);
		assertTrue(cardList.get(0).getName().equals("superman"));
		assertTrue(cardList.get(0).getDescription().equals("super carte"));
		assertTrue(cardList.get(0).getAttack() == 50);
		assertTrue(cardList.get(0).getDefence() == 50);
		assertTrue(cardList.get(0).getHp() == 100);
		assertTrue(cardList.get(0).getEnergy() == 50);
		assertTrue(cardList.get(0).getPrice() == 100);
		assertTrue(cardList.get(0).getFamily().equals("heroes"));
		assertTrue(cardList.get(0).getImgUrl().equals("http://url.com"));
	}
	
	@Test
	public void findByName() {
		crepo.save(new Card("batman", "super", "heroes", 50, 1000, 100, 50, "http://url2.com", 1000));
		crepo.save(new Card("superman", "super man carte", "heroes", 10, 10, 10, 50, "http://superman.com", 1000));
		crepo.save(new Card("superman", "super man 2", "heroes", 100, 1000, 100, 50, "http://url.com", 1000));
		List<Card> cardList = new ArrayList<>();
		crepo.findByName("superman").forEach(cardList::add);
		assertTrue(cardList.size() == 3);

	}
}
