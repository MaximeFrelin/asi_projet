package cardservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cardservice.model.Card;
import cardservice.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Integer> {
	
	public Optional<Inventory> findByUserId(int userId);
}

