package cardservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cardservice.model.Inventory;
import cardservice.repository.InventoryRepository;

@Service
public class InventoryService {
	@Autowired
	InventoryRepository iRepository;

	public void put(Inventory i) {
		iRepository.save(i);
	}
	
	public Inventory get(int userId) {
		Optional<Inventory> cOpt = iRepository.findByUserId(userId);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
}
