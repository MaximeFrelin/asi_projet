package cardservice.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cardservice.model.Card;
import cardservice.repository.CardRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cRepository;
	public void addCard(Card c) {
		Card createdCard = cRepository.save(c);
		System.out.println(createdCard);
	}
	
	public List<Card> getAll() {
		Iterator<Card> it = cRepository.findAll().iterator();
		List<Card> cards = new ArrayList<Card>();
		it.forEachRemaining(cards::add);
		
		if (!cards.isEmpty()) {
			return cards;
		}else {
			return null;
		}
	}
	
	public Card getCard(int id) {
		Optional<Card> cOpt = cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}

	public List<Card> searchCard(String name) {
		List<Card> cOpt = cRepository.findByName(name);
		if (!cOpt.isEmpty()) {
			return cOpt;
		}else {
			return null;
		}
	}
}
