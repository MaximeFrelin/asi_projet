package cardservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cardservice.model.Card;
import cardservice.model.Inventory;
import cardservice.service.CardService;
import cardservice.service.InventoryService;

@RestController
public class RestInventory {
	@Autowired
	InventoryService iService;
	
	@RequestMapping(method=RequestMethod.POST,value="/inventory/update")
    public void update(@RequestBody Inventory inventory) {
        iService.put(inventory);
    }
	
	@RequestMapping(method=RequestMethod.GET,value="/inventory/{user_id}")
    public Inventory get(@PathVariable int user_id) {
        Inventory inventory = iService.get(user_id);
        return inventory;
    }
}
