package cardservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cardservice.model.Card;
import cardservice.service.CardService;

@RestController
public class RestCard {
	@Autowired
	CardService cService;
	
	@RequestMapping(method=RequestMethod.POST,value="/card/add")
    public void addCard(@RequestBody Card card) {
        cService.addCard(card);
    }
	
	@RequestMapping(method=RequestMethod.GET,value="/card/list")
    public List<Card> getAll() {
        List<Card> cards = cService.getAll();
        return cards;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/{id}")
    public Card getCard(@PathVariable String id) {
    	Card c = cService.getCard(Integer.valueOf(id));
        return c;
    }

    @RequestMapping(method=RequestMethod.GET,value="/card/search/{name}")
    public List<Card> searchCard(@PathVariable String name) {
    	List<Card> cards = cService.searchCard(name);
        return cards;
    }
}
