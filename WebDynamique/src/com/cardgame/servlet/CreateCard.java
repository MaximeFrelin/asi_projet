package com.cardgame.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cardgame.controler.CardDao;
import com.cardgame.model.CardModel;

/**
 * Servlet implementation class Pages
 */
@WebServlet("/Pages/Cards/Create")
public class CreateCard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateCard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.getRequestDispatcher("/CreateCard.jsp").forward(request, response);
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			String name = request.getParameter("card_name");
			String description = request.getParameter("card_description");
			String family = request.getParameter("card_family");
			int hp = Integer.parseInt(request.getParameter("card_hp"));
			int energy= Integer.parseInt(request.getParameter("card_energy"));
			int defense = Integer.parseInt(request.getParameter("card_defence"));
			int attack = Integer.parseInt(request.getParameter("card_attack"));
			String imgUrl = request.getParameter("card_img_url");
			
			CardModel cardModel = new CardModel(name, description, family, hp, energy, defense, attack, imgUrl);
			CardDao dao = new CardDao();
			
			dao.addCard(cardModel);
			
			doGet(request, response);
		} catch (Exception e) {
		}
	}

}
