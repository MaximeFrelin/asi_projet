package com.cardgame.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cardgame.controler.CardDao;
import com.cardgame.model.CardModel;
import java.util.List;

/**
 * Servlet implementation class DisplayCards
 */
@WebServlet("/Pages/Cards")
public class DisplayCards extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayCards() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//Récupération de la base via DAO
		//Ajout des objets en attributes dans le JSP
		//request.setAttribute("cardList", cardList);
		
        request.getRequestDispatcher("/DisplayCards.jsp").forward(request, response);
        
        String recherche = request.getParameter("Recherche");
        CardDao dao = new CardDao();
        if(recherche != null)
        {
        	CardModel cardModelName = dao.getCardByName(recherche);
        	request.setAttribute("cardByName", cardModelName);
        	
        } else
        {
        	List<CardModel> allCardsModel = dao.getCards();
        	request.setAttribute("allCards", allCardsModel);
        }
        
        

	}
}
