$(document).ready(function () {
    console.log("test");
    $("#add").click(function () {
        let newCard = {
            name: $("#name").val(),
            description: $("#description").val(),
            imgURL: $("#imgURL").val(),
            family: $("#family").val(),
            hp: $("#hp").val(),
            energy: $("#energy").val(),
            attack: $("#attack").val(),
            defence: $("#defence").val(),
        }
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/WebServiceCard/rest/servicescard/add",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(newCard)
        }).done(function( data ) {
            alert( "Carte ajoutée " + data );
        });
    })
})
