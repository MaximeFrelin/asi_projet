package userservice.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class UserAccountTest {
	private List<String> stringList;
	private List<Integer> intList;

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add Card to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
	}
	
	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN hero list");
		stringList = null;
		intList = null;
	}
	
	@Test
	public void createCard() {
		for(String msg:stringList) {
			for(String msg2:stringList) {
				for(String msg3:stringList) {
					for(Integer msg4:intList) {
						UserAccount acc = new UserAccount(msg4, msg, msg2, msg3, msg, msg4);
						System.out.println("msg:"+msg+", msg2:"+msg2+", msg3:"+msg3+", msg4:"+msg4);
						assertTrue(acc.getId() == msg4.intValue());
						assertTrue(acc.getFirstname() == msg);
						assertTrue(acc.getLastname() == msg2);
						assertTrue(acc.getLogin() == msg3);
						assertTrue(acc.getPassword() == msg);
						assertTrue(acc.getMoney() == msg4.intValue());
					}	
				}	
			}
		}
	}

}
