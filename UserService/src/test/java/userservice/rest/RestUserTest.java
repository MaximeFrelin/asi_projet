package userservice.rest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import userservice.model.UserAccount;
import userservice.service.UserService;


@RunWith(SpringRunner.class)
@WebMvcTest(value = RestUser.class)
public class RestUserTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService uService;
	
	UserAccount user = new UserAccount(1, "firstname", "lastname", "user1", "passw0rd", 100);
	UserAccount user2 = new UserAccount(2, "us2", "lastus", "useer2", "PaSsw0rd", 1000);
	
	@Test
	public void retrieveOneUser() throws Exception {

		Mockito.when(uService.getUser(Mockito.anyInt())).thenReturn(user);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/1").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
		String expectedResult = "{\"id\":1,\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"login\":\"user1\",\"password\":\"passw0rd\",\"money\":100}";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
	
	@Test
	public void searchUser() throws Exception {

		Mockito.when(uService.searchUser(Mockito.anyString())).thenReturn(user);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/search/username").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse().getContentAsString());
		String expectedResult = "{\"id\":1,\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"login\":\"user1\",\"password\":\"passw0rd\",\"money\":100}";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
		
	}
	
}
