package userservice.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import userservice.model.UserAccount;
import userservice.repository.UserRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserService.class)
public class UserServiceTest {
	@Autowired
	private UserService uService;
	
	@MockBean
	private UserRepository uRepository;
	

	Optional<UserAccount> user = Optional.ofNullable(new UserAccount(1, "firstname", "lastname", "user1", "passw0rd", 100));
	Optional<UserAccount> user2 = Optional.ofNullable(new UserAccount(2, "us2", "lastus", "useer2", "PaSsw0rd", 1000));
	
	@Test
	public void checkUser() {
			
		Mockito.when(uRepository.findByLogin(Mockito.anyString())).thenReturn(user);
		
		int result = uService.checkUser("user1", "passw0rd");
		
		assertTrue(result == 1);
	}
	
	@Test
	public void searchUser() {
			
		Mockito.when(uRepository.findByLogin(Mockito.anyString())).thenReturn(user);
		
		UserAccount result = uService.searchUser("user1");
		
		assertTrue(result.getId() == user.get().getId());
		assertTrue(result.getLastname().equals(user.get().getLastname()));
		assertTrue(result.getFirstname().equals(user.get().getFirstname()));

	}
	
	@Test
	public void getUser() {
			
		Mockito.when(uRepository.findById(Mockito.anyInt())).thenReturn(user);
		
		UserAccount result = uService.getUser(1);
		assertTrue(result.getId() == user.get().getId());
		assertTrue(result.getLastname().equals(user.get().getLastname()));
		assertTrue(result.getFirstname().equals(user.get().getFirstname()));

	}
	
}
