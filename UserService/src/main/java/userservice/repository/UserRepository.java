package userservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import userservice.model.UserAccount;

public interface UserRepository extends CrudRepository<UserAccount, Integer> {

	public Optional<UserAccount> findByLogin(String login);
}
