package userservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import userservice.model.UserAccount;
import userservice.service.UserService;

@RestController
public class RestUser {
	@Autowired
	UserService uService;
	
	@RequestMapping(method=RequestMethod.POST,value="/user/create")
    public void addUser(@RequestBody UserAccount user) {
        uService.put(user);
    }
	
	@RequestMapping(method=RequestMethod.POST,value="/user/update")
    public void updateUser(@RequestBody UserAccount user) {
        uService.put(user);
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/user/{id}")
    public UserAccount getUser(@PathVariable String id) {
    	UserAccount u = uService.getUser(Integer.valueOf(id));
        return u;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/user/search/{login}")
    public UserAccount searchUser(@PathVariable String login) {
    	UserAccount u = uService.searchUser(login);
        return u;
    }
}
