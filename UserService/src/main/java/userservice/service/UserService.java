package userservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import userservice.model.UserAccount;
import userservice.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository uRepository;
	
	public void put(UserAccount u) {
		UserAccount createdCard = uRepository.save(u);
		System.out.println(createdCard);
	}
	
	public UserAccount getUser(int id) {
		Optional<UserAccount> cOpt = uRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
	public UserAccount searchUser(String login) {
		Optional<UserAccount> cOpt = uRepository.findByLogin(login);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
	public int checkUser(String login, String pswd) {
		Optional<UserAccount> cOpt = uRepository.findByLogin(login);
		if (cOpt.isPresent()) {
			UserAccount user = cOpt.get();
			if(user.getPassword().equals(pswd)) {
				return user.getId();
			} else {
				return -1;
			}
		}else {
			return -1;
		}
	}
}
