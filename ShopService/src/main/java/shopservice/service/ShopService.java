package shopservice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import shopservice.model.Inventory;
import shopservice.model.Card;
import shopservice.model.UserAccount;

@Service
public class ShopService {
	private final String baseUrlGetCard = "http://127.0.0.1:8082/card/{id}";
	private final String baseUrlGetInventory = "http://127.0.0.1:8082/inventory/{user_id}";
	private final String baseUrlSetInventory = "http://127.0.0.1:8082/inventory/update";
	private final String baseUrlGetUser = "http://127.0.0.1:8081/user/{id}";
	private final String baseUrlSetUser = "http://127.0.0.1:8081/user/update";

	private final RestTemplate restTemplate;

    public ShopService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    
	public Inventory buy(int idCard, int idUser) {
		Card card = this.restTemplate.getForObject(this.baseUrlGetCard, Card.class, idCard);
		Inventory inventory = this.restTemplate.getForObject(this.baseUrlGetInventory, Inventory.class, idUser);
		UserAccount user = this.restTemplate.getForObject(this.baseUrlGetUser, UserAccount.class, idUser);
		
		if(user.getMoney() >= card.getPrice()) {
			List<Card> cards;
			if(inventory == null) {
				inventory = new Inventory();
				inventory.setUserId(idUser);
				cards = new ArrayList<Card>();
			} else {
				cards = inventory.getCards();
			}
			cards.add(card);
			inventory.setCards(cards);
			user.setMoney(user.getMoney()-card.getPrice());
			
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		    
		    HttpEntity<Inventory> inv = new HttpEntity<>(inventory, headers);

		    restTemplate.postForObject(this.baseUrlSetInventory, inv, Inventory.class);
		    
		    headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		    
		    HttpEntity<UserAccount> us = new HttpEntity<>(user, headers);

		    restTemplate.postForObject(this.baseUrlSetUser, us, UserAccount.class);
		    
		    return inventory;
			
		} else {
			return null;
		}
	}
	
	public Inventory sell(int idCard, int idUser) {
		Card card = this.restTemplate.getForObject(this.baseUrlGetCard, Card.class, idCard);
		Inventory inventory = this.restTemplate.getForObject(this.baseUrlGetInventory, Inventory.class, idUser);
		UserAccount user = this.restTemplate.getForObject(this.baseUrlGetUser, UserAccount.class, idUser);
		int idCardFound = -1;
		
		if(inventory == null) {
			return null;
		}
		
		List<Card> cards = inventory.getCards();
		for(int i = 0;i < cards.size();i++) {
			if(cards.get(i).getId() == card.getId()) {
				idCardFound = i;
			}
		}
		
		if(idCardFound != -1) {
			cards.remove(idCardFound);
			inventory.setCards(cards);
			user.setMoney(user.getMoney()+card.getPrice());
			
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		    
		    HttpEntity<Inventory> inv = new HttpEntity<>(inventory, headers);

		    restTemplate.postForObject(this.baseUrlSetInventory, inv, Inventory.class);
		    
		    headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		    
		    HttpEntity<UserAccount> us = new HttpEntity<>(user, headers);

		    restTemplate.postForObject(this.baseUrlSetUser, us, UserAccount.class);
		    
		    return inventory;
		} else {
			return null;
		}
	}
}
