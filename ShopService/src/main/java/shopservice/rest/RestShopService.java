package shopservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import shopservice.model.Card;
import shopservice.model.Inventory;
import shopservice.service.ShopService;
import shopservice.transport.BuyTransport;

@RestController
public class RestShopService {
	@Autowired
	ShopService sService;
	
	@RequestMapping(method=RequestMethod.POST,value="/shop/buy")
    public Inventory buy(@RequestBody BuyTransport buyTransport) {
        return sService.buy(buyTransport.getIdCard(), buyTransport.getIdUser());
    }
	
	@RequestMapping(method=RequestMethod.POST,value="/shop/sell")
    public Inventory sell(@RequestBody BuyTransport buyTransport) {
        return sService.sell(buyTransport.getIdCard(), buyTransport.getIdUser());
    }
}
