package shopservice.transport;

public class BuyTransport {
	private int idCard;
	private int idUser;
	
	public BuyTransport(int idCard, int idUser) {
		super();
		this.idCard = idCard;
		this.idUser = idUser;
	}
	
	public BuyTransport() {
		
	}

	public int getIdCard() {
		return idCard;
	}

	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
