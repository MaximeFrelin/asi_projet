package shopservice.model;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
	
	private int id;
	private int userId;
	private List<Card> cards;
	
	public Inventory(int id, List<Card> cards, int userId) {
		super();
		this.id = id;
		this.cards = cards;
		this.userId = userId;
	}
	
	public Inventory() {
		this.cards = new ArrayList<Card>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
