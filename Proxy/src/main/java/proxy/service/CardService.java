package proxy.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import proxy.transport.CardTransport;

@Service
public class CardService {
	private final String baseUrlAddCard = "http://127.0.0.1:8082/card/add";
	private final String baseUrlGetAllCard = "http://127.0.0.1:8082/card/list";
	private final String baseUrlGetCard = "http://127.0.0.1:8082/card/{id}";
	private final String baseUrlSearchCard = "http://127.0.0.1:8082/card/search/{name}";
	
	private final RestTemplate restTemplate;
	
	public CardService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
	
	public CardTransport addCard(CardTransport userTransport) {
		String url = baseUrlAddCard;
		
		HttpHeaders headers = new HttpHeaders();
	    // set `content-type` header
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    // set `accept` header
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put("userTransport", userTransport);
	    
	    HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
	    
	    ResponseEntity<CardTransport> response = this.restTemplate.postForEntity(url, entity, CardTransport.class);

	    // check response status code
	    if (response.getStatusCode() == HttpStatus.CREATED) {
	        return response.getBody();
	    } else {
	        return null;
	    }
	}
	
	public List<CardTransport> getAll() {
		CardTransport cards[] = this.restTemplate.getForObject(this.baseUrlGetAllCard, CardTransport[].class);
		return Arrays.asList(cards);
	}
	
	public CardTransport getCard(int id) {
		return this.restTemplate.getForObject(this.baseUrlGetCard, CardTransport.class, id);
	}
	
	public List<CardTransport> searchCard(String name) {
		CardTransport cards[] = this.restTemplate.getForObject(this.baseUrlSearchCard, CardTransport[].class, name);
		return Arrays.asList(cards);
	}
}
