package proxy.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import proxy.transport.AuthTransport;
import proxy.transport.UserAccountTransport;

@Service
public class AuthService {
	private final String baseUrlAuth = "http://127.0.0.1:8083/user/login";
	
	private final RestTemplate restTemplate;
	
	public AuthService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
	
	public UserAccountTransport auth(AuthTransport userTransport) {
		String url = baseUrlAuth;
		
		HttpHeaders headers = new HttpHeaders();
	    // set `content-type` header
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    // set `accept` header
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put("userTransport", userTransport);
	    
	    HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
	    
	    ResponseEntity<UserAccountTransport> response = this.restTemplate.postForEntity(url, entity, UserAccountTransport.class);

	    // check response status code
	    if (response.getStatusCode() == HttpStatus.CREATED) {
	        return response.getBody();
	    } else {
	        return null;
	    }
	}
}
