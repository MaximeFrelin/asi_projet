package proxy.service;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import proxy.transport.InventoryTransport;

@Service
public class InventoryService {
	private final String baseUrlGetInventory = "http://127.0.0.1:8082/inventory/{user_id}";
	
	private final RestTemplate restTemplate;
	
	public InventoryService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
	
	public InventoryTransport getInventory(int user_id) {
		return this.restTemplate.getForObject(this.baseUrlGetInventory, InventoryTransport.class, user_id);
	}
}
