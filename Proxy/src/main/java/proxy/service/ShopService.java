package proxy.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import proxy.transport.BuyTransport;
import proxy.transport.InventoryTransport;

@Service
public class ShopService {
	private final String baseUrlBuy = "http://127.0.0.1:8084/shop/buy";
	private final String baseUrlSell = "http://127.0.0.1:8084/shop/sell";
	
	private final RestTemplate restTemplate;

    public ShopService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    
    public InventoryTransport buy(BuyTransport buyTransport) {
    	String url = this.baseUrlBuy;
		
		HttpHeaders headers = new HttpHeaders();
	    // set `content-type` header
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    // set `accept` header
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put("buyTransport", buyTransport);
	    
	    HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
	    
	    ResponseEntity<InventoryTransport> response = this.restTemplate.postForEntity(url, entity, InventoryTransport.class);

	    // check response status code
	    if (response.getStatusCode() == HttpStatus.CREATED) {
	        return response.getBody();
	    } else {
	        return null;
	    }
    }
    
    public InventoryTransport sell(BuyTransport buyTransport) {
    	String url = this.baseUrlSell;
		
		HttpHeaders headers = new HttpHeaders();
	    // set `content-type` header
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    // set `accept` header
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put("buyTransport", buyTransport);
	    
	    HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
	    
	    ResponseEntity<InventoryTransport> response = this.restTemplate.postForEntity(url, entity, InventoryTransport.class);

	    // check response status code
	    if (response.getStatusCode() == HttpStatus.CREATED) {
	        return response.getBody();
	    } else {
	        return null;
	    }
    }
}
