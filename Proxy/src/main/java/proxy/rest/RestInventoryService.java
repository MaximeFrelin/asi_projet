package proxy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import proxy.service.InventoryService;
import proxy.transport.InventoryTransport;

@RestController
public class RestInventoryService {
	@Autowired
	InventoryService iService;
	
	@RequestMapping(method=RequestMethod.GET,value="/inventory/{user_id}")
    public InventoryTransport get(@PathVariable int user_id) {
		return iService.getInventory(user_id);
    }
}
