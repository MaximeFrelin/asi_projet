package proxy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import proxy.transport.CardTransport;
import proxy.transport.InventoryTransport;
import proxy.service.ShopService;
import proxy.transport.BuyTransport;

@RestController
public class RestShopService {
	@Autowired
	ShopService sService;
	
	@RequestMapping(method=RequestMethod.POST,value="/shop/buy")
    public InventoryTransport buy(@RequestBody BuyTransport buyTransport) {
        return sService.buy(buyTransport);
    }
	
	@RequestMapping(method=RequestMethod.POST,value="/shop/sell")
    public InventoryTransport sell(@RequestBody BuyTransport buyTransport) {
        return sService.sell(buyTransport);
    }
}
