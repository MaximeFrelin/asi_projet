package proxy.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import proxy.transport.UserAccountTransport;
import proxy.service.AuthService;
import proxy.transport.AuthTransport;

@RestController
public class RestAuthService {
	@Autowired
	AuthService aService;
	
    @RequestMapping(method=RequestMethod.POST,value="/user/login")
    public UserAccountTransport searchUser(@RequestBody AuthTransport userTransport) {
    	return aService.auth(userTransport);
    }
}
