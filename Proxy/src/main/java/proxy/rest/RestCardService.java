package proxy.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import proxy.transport.CardTransport;
import proxy.service.CardService;

@RestController
public class RestCardService {
	@Autowired
	CardService cService;
	
	@RequestMapping(method=RequestMethod.POST,value="/card/add")
    public CardTransport addCard(@RequestBody CardTransport card) {
        return cService.addCard(card);
    }
	
	@RequestMapping(method=RequestMethod.GET,value="/card/list")
    public List<CardTransport> getAll() {
        List<CardTransport> cards = cService.getAll();
        return cards;
    }
    
    @RequestMapping(method=RequestMethod.GET,value="/card/{id}")
    public CardTransport getCard(@PathVariable String id) {
    	CardTransport c = cService.getCard(Integer.valueOf(id));
        return c;
    }

    @RequestMapping(method=RequestMethod.GET,value="/card/search/{name}")
    public List<CardTransport> searchCard(@PathVariable String name) {
    	List<CardTransport> cards = cService.searchCard(name);
        return cards;
    }
}
