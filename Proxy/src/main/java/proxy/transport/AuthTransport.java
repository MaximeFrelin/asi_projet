package proxy.transport;

import java.io.Serializable;

public class AuthTransport implements Serializable {
	
	private String login;
	private String paswd;
	
	public AuthTransport(String login, String paswd) {
		super();
		this.login = login;
		this.paswd = paswd;
	}
	
	public AuthTransport() {
		
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPaswd() {
		return paswd;
	}

	public void setPaswd(String paswd) {
		this.paswd = paswd;
	}
	
}
