package proxy.transport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InventoryTransport implements Serializable {
	
	private int id;
	private List<CardTransport> cards;
	private int userId;
	
	public InventoryTransport(int id, List<CardTransport> cards, int user_id) {
		super();
		this.id = id;
		this.cards = cards;
		this.userId = user_id;
	}
	
	public InventoryTransport() {
		this.cards = new ArrayList<CardTransport>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<CardTransport> getCards() {
		return cards;
	}

	public void setCards(List<CardTransport> cards) {
		this.cards = cards;
	}

	public int getUser_id() {
		return userId;
	}

	public void setUser_id(int user_id) {
		this.userId = user_id;
	}
}
