class CardService extends Service {
    
    async getCards() {
        let url = "cards/list";
        let data = {};
        let response = await super.get(url, data);
    
        return response.data;
      }
}