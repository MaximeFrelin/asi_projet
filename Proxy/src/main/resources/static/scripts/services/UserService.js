class UserService extends Service {
  async login(login, password) {
    let url = "user/login";
    let data = {
      login,
      password,
    };
    let response = await super.get(url, data);

    return response.data;
  }

  async register(login, password, firstname, lastname) {
    let url = "user/create";
    let data = {
      id: null,
      login,
      password,
      firstname,
      lastname,
    };
    let response = await super.post(url, data);
    return response.data;
  }
}
