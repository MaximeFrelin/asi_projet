var userService = new UserService();

function launch() {
  this.initEventListener();
}

function initEventListener() {
  $("#submitLogin").on("click", onSubmitLogin);
  $("#createAccountBtn").on("click", onCreateAccountBtn);
}

async function onSubmitLogin(event) {
  let data = await this.userService?.getUser();
  handleUserLoginResult(data);
}

function onCreateAccountBtn() {
  window.location.href = "userRegister.html";
}

function handleUserLoginResult(data) {
  if (data) {
    //Redirection
  }
}
