$(document).ready(function () {
    console.log("test");
    $("#search").click(function () {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/WebServiceCard/rest/servicescard/find",
            contentType: "application/json; charset=utf-8",
            data: { name: $("#searchField").val() },
            dataType: "json"
        }).done(function( data ) {
            if(data) {
                $("#name").val(data.name);
                $("#description").val(data.description);
                $("#imgURL").attr('src', data.imgUrl);
                $("#family").val(data.family);
                $("#hp").val(data.hp);
                $("#energy").val(data.energy);
                $("#attack").val(data.attack);
                $("#defence").val(data.defence);
            }
        });
    })
})
