import {cardService} from ".../services/cardService.js";

async function launch() {

    loadData();


   


}

async function loadData()
{
    let cards = JSON.parse(await cardService.getCards());
    let tableRef = document.getElementById('tableCards');
    let theadRef = tableRef.getElementsByTagName('thead')[0];

    let enteteCard = Object.keys(cards);
    let enteteRow = theadRef.insertRow(0);

    for(let i = 0; i < enteteCard.length; i++)
    {
        enteteRow.insertCell(i).appendChild(enteteCard[i]);
    }


    let tbodyRef = tableRef.getElementsByTagName('tbody')[0];

    for(let i = 0; i < cards.length; i++)
    {
        var currentRow = tbodyRef.insertRow(i);

            let cellule = currentRow.insertCell(j);

            Object.keys(cards[i]).forEach((key)=>{
                cellule.appendChild(cards[i][key]);
            })
         
    }
}
