var userService = new UserService();

function launch() {
  initEvent();
}

function initEvent() {
  $("#registerForm").submit(() => {
    onSubmitRegisterEvent();
    return false;
  });
}

function onSubmitRegisterEvent(event) {
  let login = $("#login").val();
  let password = $("#password").val();
  let firstName = $("#firstName").val();
  let lastName = $("#lastName").val();

  userService
    .register(login, password, firstName, lastName)
    .then((data) => {
      $("#errorContainer").hide();
      if (data != null) {
        window.location.href = "userConnection.html";
      }
    })
    .catch((e) => {
      console.error(e);
      $("#errorContainer").show();
    });
}
