class Service {
  BASE_URL = "localhost:8081/";

  get computedUrl() {
    return (url) => {
      return this.BASE_URL + url;
    };
  }

  get(url, data) {
    return this.request(url, data, "GET");
  }

  post(url, data) {
    return this.request(url, data, "POST");
  }

  request(url, data, method) {
    return $.ajax({
      type: method,
      url: this.computedUrl(url),
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(data),
    });
  }
}
