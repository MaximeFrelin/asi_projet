package authservice.transport;

public class UserAuthTransport {
	private String login;
	private String pswd;
	
	public UserAuthTransport(String login, String pswd) {
		super();
		this.login = login;
		this.pswd = pswd;
	}
	
	public UserAuthTransport() {
		
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPswd() {
		return pswd;
	}
	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	
}
