package authservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import authservice.model.UserAccount;
import authservice.service.AuthService;
import authservice.transport.UserAuthTransport;

@RestController
public class RestAuthService {
	@Autowired
	AuthService uService;
    
    @RequestMapping(method=RequestMethod.POST,value="/user/login")
    public UserAccount searchUser(@RequestBody UserAuthTransport userTransport) {
    	UserAccount u = uService.login(userTransport.getLogin(), userTransport.getPswd());
        return u;
    }
}
