package authservice.service;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import authservice.model.UserAccount;
import authservice.transport.UserAuthTransport;

@Service
public class AuthService {
	
	private final RestTemplate restTemplate;

    public AuthService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }
    
	public UserAccount login(String login, String pswd) {
		String url = "http://127.0.0.1:8081/user/search/{login}";
		UserAccount user = this.restTemplate.getForObject(url, UserAccount.class, login);
		
		if(user.getPassword().equals(pswd)) {
			return user;
		} else {
			return null;
		}
	}
}
